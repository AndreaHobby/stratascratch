# Import your libraries
import pandas as pd

# Start writing code
az_employees.head()

az_employees.groupby('department_name').size().reset_index()

x = az_employees[ (az_employees['department_name'] == 'Human Resources') | (az_employees['department_name'] == 'Sales')]  

df = x[x['position'].str.contains("Manager",  case=False)]

df[["first_name", "last_name"]] 
