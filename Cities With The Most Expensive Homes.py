# Import your libraries
import pandas as pd

# Start writing code
zillow_transactions.head()

natavg = zillow_transactions['mkt_price'].mean()

df = zillow_transactions.groupby('city').aggregate({'mkt_price':'mean'}).reset_index() 

df2 = df[df['mkt_price'] > natavg]

df2[["city"]] 
