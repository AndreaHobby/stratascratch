# Import your libraries
import pandas as pd
import numpy as np

# Start writing code
uber_advertising.head()

uber_advertising.info()

df = uber_advertising[(uber_advertising['year'] >= 2017) & (uber_advertising['year']<= 2018)] 

df = df.groupby(['advertising_channel'])['money_spent', 'customers_acquired'].sum().reset_index()

df['avgeff'] = (df['money_spent'] /df['customers_acquired'])

test = df.sort_values(['avgeff'], ascending=[True]) 

test[['advertising_channel', 'avgeff']]
