# Import your libraries
import pandas as pd

# Start writing code
worker.head()

df = worker[(worker['salary'] >= 50000) & (worker['salary'] <= 100000)]

df1 = df[["first_name", "last_name", "salary"]]  

#combine first and last name column into new column in df, with space in between 
df1['full_name'] = df1['first_name'] + ' ' + df1['last_name']

df1[['full_name', 'salary']] 
