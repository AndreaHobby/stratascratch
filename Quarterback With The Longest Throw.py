# Import your libraries
import pandas as pd

# Start writing code
qbstats_2015_2016.head()

df = qbstats_2015_2016[(qbstats_2015_2016['year'] == 2016)]  

df1=df.sort_values(['lg'], ascending=[False])

df2= df1[["qb", "lg"]]

df2['lg_num'] = df2['lg'].str.extract('(\d+)', expand=False)

df3= df2[["qb", "lg_num"]]

df3.head(1)
