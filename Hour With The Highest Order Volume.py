# Import your libraries
import pandas as pd

# Start writing code
postmates_orders.head()

postmates_orders['hour']=postmates_orders.order_timestamp_utc.dt.strftime('%H') 

postmates_orders.head()
postmates_orders['date'] = pd.to_datetime(
    postmates_orders['order_timestamp_utc']).dt.date
    
result = postmates_orders.groupby([postmates_orders['date'], postmates_orders['hour']])[
    'id'].count().to_frame('n_orders').reset_index()
result = result.groupby('hour')['n_orders'].mean().reset_index()
result[result['n_orders'] == result['n_orders'].max()]
