# Import your libraries
import pandas as pd

# Start writing code
los_angeles_restaurant_health_inspections.head()

#convert datetime column to just date
los_angeles_restaurant_health_inspections['new_activity_date'] = pd.to_datetime(los_angeles_restaurant_health_inspections['activity_date']).dt.date 

df1 = los_angeles_restaurant_health_inspections.groupby('new_activity_date', as_index=False)['score'].count()

df1.sort_values(['new_activity_date'], ascending=[True])
